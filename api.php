<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
header('Access-Control-Allow-Origin : *');
header('Access-Control-Allow-Headers : Content-Type,X-Auth-Token,Authorization,Origin');
header('Access-Control-Allow-Methods :GET, POST, PUT, DELETE, OPTIONS');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'ApiController@login');
Route::post('register', 'ApiController@register');
Route::group(['middleware' => 'auth.jwt'], function () {
    Route::get('logout', 'ApiController@logout');
    Route::get('user', 'ApiController@getAuthUser');
    Route::post('user/update', 'ApiController@updateUser');
    Route::delete('user/delete', 'ApiController@deleteCurrentUser');
    Route::get('expenses', 'ExpenseController@index');
    Route::get('expense/{id}', 'ExpenseController@show');
    Route::post('expense', 'ExpenseController@store');
    Route::put('expense/{id}', 'ExpenseController@update');
    Route::delete('expense/{id}', 'ExpenseController@destroy');
    Route::get('{expenseid}/participants', 'ParticipantController@index');
    Route::get('{expenseid}/participant/{id}', 'ParticipantController@show');
    Route::post('{expenseid}/participant', 'ParticipantController@store');
    Route::put('{expenseid}/participant/{id}', 'ParticipantController@update');
    Route::delete('{expenseid}/participant/{id}', 'ParticipantController@destroy');

});