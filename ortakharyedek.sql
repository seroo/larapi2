-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 26, 2019 at 08:49 AM
-- Server version: 5.7.23
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `ortakhar_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE `expenses` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(50) COLLATE utf8mb4_turkish_ci NOT NULL,
  `variety` varchar(20) COLLATE utf8mb4_turkish_ci NOT NULL,
  `place` varchar(20) COLLATE utf8mb4_turkish_ci NOT NULL,
  `isJoint` blob NOT NULL,
  `amount` double(8,2) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_turkish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_turkish_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_turkish_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_02_22_201628_create_expenses_table', 1),
(4, '2019_02_22_202444_create_participants_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `participants`
--

CREATE TABLE `participants` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(20) COLLATE utf8mb4_turkish_ci NOT NULL,
  `description` varchar(50) COLLATE utf8mb4_turkish_ci NOT NULL,
  `isJoint` blob NOT NULL,
  `amountOfJoint` double(8,2) NOT NULL,
  `amountOfCut` double(8,2) NOT NULL,
  `isPaid` blob NOT NULL,
  `expense_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_turkish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_turkish_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_turkish_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_turkish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_turkish_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_turkish_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_turkish_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_turkish_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_turkish_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'sero', 'ss@dd.com', '2019-02-22 23:37:57', '1234', 'aa', '2019-02-22 23:37:57', '2019-02-22 23:37:57'),
(2, 'seroo', 'aa@bb.com', NULL, '$2y$10$ce3zXzDpe2gdYq2oJvzbbeyB12UGn1GTr1ux9tfnnAhEmIc6pbBNC', NULL, '2019-02-22 20:48:14', '2019-02-22 20:48:14'),
(3, 'ds', 'ss', NULL, '$2y$10$VQ/oMOiHDyCX1/mLD/sfYO/KAcGQsXjMvWctDn/4CH6XUpnWl1jfm', NULL, '2019-02-24 11:31:00', '2019-02-24 11:31:00'),
(15, '11', '11', NULL, '$2y$10$NtYp7Oo/xgtu2l0uPKOm4uniSoeb3Xlvthr3h9SjS4B1k..i9UOAm', NULL, '2019-02-24 15:49:20', '2019-02-24 15:49:20'),
(16, '22', '22', NULL, '$2y$10$IuSSQaYl50ABaeFQnH0u/OOo.183bp92ecHoz2a/tgRYOFYHHMlnu', NULL, '2019-02-24 15:55:20', '2019-02-24 15:55:20'),
(26, 'aa', 'sss', NULL, '$2y$10$bflZNhK6wxHi8zSlgM.Bjuej5UB2zlj4/jZ3DfOo6j35a8UiDtAqW', NULL, '2019-02-24 16:51:40', '2019-02-24 16:51:40'),
(45, 'ss', 'ssss', NULL, '$2y$10$y6oKRDDnDJCaQk1z738pQ.p2MIKrV8RQ/PsGb/LAmaNi2oEpNrxfm', NULL, '2019-02-24 17:45:43', '2019-02-24 17:45:43'),
(47, '1', '1', NULL, '$2y$10$SQfv8LZZC/WIAL89igEq3ule6C9jSNA8qu2rEM9V5utXzkFO/y15O', NULL, '2019-02-24 17:50:50', '2019-02-24 17:50:50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `participants`
--
ALTER TABLE `participants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `participants`
--
ALTER TABLE `participants`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
