<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RegisterAuthRequest;
use App\User;
use Illuminate\Support\Facades\Hash;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;


class ApiController extends Controller
{
    public $loginAfterSignUp = true;

    public function register(RegisterAuthRequest $request) {
        $request->validate([
            'name' => 'min:2|required|max:255',
            'email' => 'required|max:255',
            'password' => 'required|min:2',
        ]);

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        if($this->loginAfterSignUp) {
            return $this->login($request);
        }

        return response()->json([
           'success' => true,
           'data' => $user
        ], 200);
    }

    public function login(Request $request) {
        $input = $request->only('email', 'password');
        $jwt_token = null;

        if(!$jwt_token = JWTAuth::attempt($input)) {
            return response()->json([
                'success' => false,
                'message' => 'Invalid Email or Password',
            ], 401);
        }
//        dd($jwt_token);
        return response()->json([
            'success' => true,
            'token' => $jwt_token,
            'user' => JWTAuth::user()
        ]);
    }

    public function logout(Request $request) {
        $this->validate($request, [
            'token' => 'required'
        ]);
        try {
            JWTAuth::invalidate($request->token);
            return response()->json([
                'success' => true,
                'message' => 'User logged out successfully'
            ]);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, the user cannot be logged out'
            ], 500);
        }
    }

    public function getAuthUser(Request $request)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);
        $user = JWTAuth::authenticate($request->token);
        return response()->json([
            'user' => $user,
            'message' => 'success'
            ]);
    }

    public function updateUser(Request $request)
    {

        $this->validate($request, [
            'token' => 'required'
        ]);
        $user = JWTAuth::authenticate($request->token);
        $this->validate($request, [
            'name' => 'min:2|max:255',
            'email' => 'email|max:255'.$user->id,

            ]);

        if ($request->password) {
            $this->validate($request, [
                'password' => 'min:2',
            ]);
            if (Hash::check($request->password, $user->password)) {
                $user->name = $request->name;
                $user->email = $request->email;
                $user->password = bcrypt($request->newPassword);
                $user->save();
            } else {
                return response()->json([
                    'success' => true,
                    'message' => 'Old password is wrong'
                ], 204);
            }
        } else {
//            echo $request->name . ' - ' . $request->email;
//            die();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->save();
        }

        return response()->json([
            'success' => true,
            'data' => $user
        ], 200);

    }

    public function deleteCurrentUser(Request $request) {
        $this->validate($request, [
            'token' => 'required'
        ]);
//        print_r($request->token);
//        die();
        if($user = JWTAuth::authenticate($request->token)) {

            if($user->delete() > 0) {
                return response()->json([
                    'success' => true,
                    'message' => 'User deleted'
                ], 200);
            }

        } else {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, user not found'
            ], 500);
        }

    }

}
