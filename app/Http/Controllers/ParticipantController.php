<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Participant;
use App\Expense;
use JWTAuth;

class ParticipantController extends Controller
{
    protected $user;

    public function __construct()
    {
        /**
         * bu user authentication icin yapilio
         * sonra user ın içindeki expenses metodu bişekil
         * expenselere ulaşıo (laravel in işleri)
         * daha sonra da bu expense modelindeki participants
         * metoduyla da participantlara erişiliyor
         * çok enteresan
         */
        $this->user = JWTAuth::parseToken()->authenticate();
    }

    public function index($expenseid)
    {
        // once user modeldeki expenses metodundan expense
        // sonra expense modeldeki participants metodu ile
        // participantlara erişiliyor
        $expense = $this->user
            ->expenses()->find($expenseid);
        if (!$expense) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, expense with id ' . $expenseid . ' cannot be found'
            ], 400);
        }
        return $expense->participants()
            ->get(['id', 'name',
                'description',
                'isJoint',
                'amountOfJoint',
                'amountOfCut',
                'isPaid',
                'expense_id'])
            ->toArray();
    }

    public function show($expenseid, $id)
    {
        $expense = $this->user
            ->expenses()->find($expenseid);
        if (!$expense) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, expense with id ' . $expenseid . ' cannot be found'
            ], 400);
        }

        return $expense->participants()
            ->find($id);

    }

    // FIXME expense id yi url parametresi olarak almak daha iyi olur
    public function store(Request $request, $expenseid)
    {


        // nullable yapmassam http 422 error alıyorum
        $this->validate($request, [
            'description' => 'string|nullable',
            'name' => 'required|string',
            'amountOfJoint' => 'numeric',
            'isJoint' => 'integer',
            'isPaid' => 'integer',
            'amountOfCut' => 'numeric',
        ]);
//        echo $request;
//        die();
        $participant = new Participant();
        $participant->description = $request->description;
        $participant->name = $request->name;
        $participant->amountOfJoint = $request->amountOfJoint;
        $participant->isJoint = $request->isJoint;
        $participant->isPaid = $request->isPaid;
        $participant->amountOfCut = $request->amountOfCut;
        $participant->expense_id = $expenseid;

        $expense = $this->user->expenses()->find($participant->expense_id);



        if (!$expense) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, expense cannot be found'
            ], 400);
        } else {
//            echo $expense->participants()->save($participant);
//            die();
            if ($expense->participants()->save($participant)) {

                return response()->json([
                    'success' => true,
                    'participant' => $participant
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, participant could not be added'
                ], 500);
            }
        }
    }

    public function update(Request $request, $expenseid, $id)
    {

        $expense = $this->user->expenses()->find($expenseid);

        if (!$expense) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, expense with id ' . $id . ' cannot be found'
            ], 400);
        }
        $participant = $expense->participants()->find($id);

        $updated = $participant->fill($request->all())
            ->save();
        if ($updated) {
            return response()->json([
                'success' => true
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, participant could not be updated'
            ], 500);
        }
    }

    public function destroy($expenseid, $id)
    {


        $expense = $this->user->expenses()->find($expenseid);

        if (!$expense) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, expense with id ' . $id . ' cannot be found'
            ], 400);
        }

//        var_dump($expense->participants()->delete($id));
//        die();
        // expense e ait participantlardan bu idliyi sil
        if ($expense->participants()->find($id)->delete()) {
            return response()->json([
                'success' => true
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => '$expense could not be deleted'
            ], 500);
        }
    }


}
