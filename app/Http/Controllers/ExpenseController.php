<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Expense;
use JWTAuth;
//use DB; query builder kullanabilmek icin

class ExpenseController extends Controller
{
    protected $user;

    public function __construct()
    {

        $this->user = JWTAuth::parseToken()->authenticate();
        // TODO su token refresh uzerine calismak lazım
//        $token = JWTAuth::getToken();
//        $newToken = JWTAuth::refresh($token);
//        dd($newToken);
//        dd($newToken);
//        date_default_timezone_set('Europe/Istanbul');
//        dd(date('d M Y h:i', JWTAuth::parseToken()->getPayload()->get('exp')));
    }

    public function index()
    {
        return $this->user
            ->expenses()
            ->get(['id',
                'description',
                'variety',
                'place',
                'isJoint',
                'amount',
                'user_id'])
            ->toArray();
    }
    // Token adres cubugundan
    public function show(Request $request, $id)
    {
        $expense = $this->user->expenses()->find($id);

        if (!$expense) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, expense with id ' . $id . ' cannot be found'
            ], 400);
        }

        return $expense;
    }
    // NOTE : Token ı body ile gonderiyo
    public function store(Request $request)
    {
        $this->validate($request, [
            'description' => 'required|string',
            'variety' => 'required|string',
            'place' => 'string|nullable',
            'isJoint' => 'integer',
            'amount' => 'required|numeric',
        ]);

        // user_id yi vermiyoruz cunku authenticate userdan direk alıo asagida eklerken
        $expense = new Expense();
        $expense->description = $request->description;
        $expense->variety = $request->variety;
        $expense->place = $request->place;
        $expense->isJoint = $request->isJoint;
        $expense->amount = $request->amount;

        /*
         * dd($request->all());dd($request->all());
         * Asagidaki metodda user üzerinden gönderdiği için
         * requestte user id farklı bile gelse kendi authenticated
         * user ından gonderiyo post u
         *
         */
        // get metodunda id yi çekmeyi ayarlayinca save sonrasi id yi de donuyor
        if ($this->user->expenses()->save($expense))
            return response()->json([
                'success' => true,
                'expense' => $expense,
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'Sorry, product could not be added'
            ], 500);
    }

    //NOTE : token ı url den gonderiyo
    // form u url encoded gondermessem olmuo
    public function update(Request $request, $id)
    {
        $expense = $this->user->expenses()->find($id);
//
        if (!$expense) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, expense with id ' . $id . ' cannot be found'
            ], 400);
        }
        $updated = $expense->fill($request->all())
            ->save();

        if ($updated) {
            return response()->json([
                'success' => true,
                'expense' => $expense
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, expense could not be updated'
            ], 500);
        }
    }

    public function destroy($id)
    {
        $expense = $this->user->expenses()->find($id);

        if (!$expense) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, expense with id ' . $id . ' cannot be found'
            ], 400);
        }

        if ($expense->delete()) {
            return response()->json([
                'success' => true,
                'message' => '$expense was deleted'
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => '$expense could not be deleted'
            ], 500);
        }
    }


}
