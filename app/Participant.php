<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
    public function expense() {
        return $this->belongsTo(Expense::class);
    }

    protected $fillable = [
        'name',
        'description',
        'isJoint',
        'amountOfJoint',
        'amountOfCut',
        'isPaid',
        'expense_id'
    ];
}
