<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $primaryKey = 'id';

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function participants() {
        return $this->hasMany(Participant::class);
    }

    protected $fillable = [
        'description',
        'variety',
        'place',
        'isJoint',
        'amount',
        'user_id'
    ];

}
